package ru.tsc.babeshko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findFirstByLogin(@NotNull String login);

}
