package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.enumerated.RoleType;
import ru.tsc.babeshko.tm.exception.field.EmptyLoginException;
import ru.tsc.babeshko.tm.exception.field.EmptyPasswordException;
import ru.tsc.babeshko.tm.model.Role;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test",  RoleType.USUAL);
    }

    private void initUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userRepository.findFirstByLogin(login);
        if (user != null) return;
        createUser(login, password, role);
    }


    @Modifying
    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        @NotNull final Role role = roleType == null ? new Role(user, RoleType.USUAL) : new Role(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findFirstByLogin(login);
    }

}
