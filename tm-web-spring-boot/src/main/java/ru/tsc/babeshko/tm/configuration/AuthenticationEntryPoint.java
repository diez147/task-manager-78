package ru.tsc.babeshko.tm.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import ru.tsc.babeshko.tm.dto.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(
            @NotNull final HttpServletRequest request,
            @NotNull final HttpServletResponse response,
            @NotNull final AuthenticationException exception
    ) throws IOException {
        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String value = exception.getMessage();
        @NotNull final Message message = new Message(value);
        writer.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("babeshko");
        super.afterPropertiesSet();
    }

}