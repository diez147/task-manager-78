package ru.tsc.babeshko.tm.exception.entity;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}