package ru.tsc.babeshko.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.marker.UnitCategory;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.UserRepository;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest {

    @NotNull
    private static final String USER_LOGIN = "user_test";

    @NotNull
    @Autowired
    private UserRepository repository;

    @NotNull
    private final User user = new User(USER_LOGIN, "test");

    @Before
    public void init() {
        repository.save(user);
    }

    @After
    @Transactional
    public void destroy() {
        repository.delete(user);
    }

    @Test
    public void findFirstByLogin() {
        @Nullable final User user = repository.findFirstByLogin(USER_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_LOGIN, user.getLogin());
    }

}