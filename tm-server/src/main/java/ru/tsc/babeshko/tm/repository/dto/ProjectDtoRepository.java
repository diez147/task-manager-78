package ru.tsc.babeshko.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.dto.model.ProjectDto;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> {

}